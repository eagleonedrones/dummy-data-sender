#!make
include .env
export $(shell sed 's/=.*//' .env)


.PHONY: build run test clean

build: keys/hub_rsa.pub
	docker-compose -f docker-compose.prod.yaml build \
	 --build-arg GIT_PRIVATE_KEY="$(shell cat ~/.ssh/id_rsa | perl -pe 's/\n/__x__/g')"

keys/hub_rsa.pub:
	curl http://${HUB_HOST}:${HUB_PORT}/public_key > keys/hub_rsa.pub

run:
	trap 'make clean' EXIT; docker-compose -f docker-compose.prod.yaml up

test:
	trap 'make clean' EXIT; docker-compose -f docker-compose.prod.yaml run dummy_server test

clean:
	docker-compose -f docker-compose.prod.yaml down --remove-orphans -v
