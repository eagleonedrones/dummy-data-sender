#!/bin/bash

set -e

# Wait until HUB is up
until wget http://${HUB_HOST}:${HUB_PORT}/version/ -O - > /dev/null; do
  echo "HUB is unavailable - sleeping"
  sleep 1
done

# Commands available using `docker-compose run dummy_server [COMMAND]`
case "$1" in
    python)
        /venv/bin/python
    ;;
    test)
        /venv/bin/python -m unittest
    ;;
    *)
        echo "Running Server..."
        /venv/bin/python runserver.py
    ;;
esac
