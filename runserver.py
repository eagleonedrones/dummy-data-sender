import asyncio
import json
import os

from jinja2 import Environment, PackageLoader
from rsa import PublicKey
from sanic import Sanic
from sanic.response import html, redirect

from client.client import HubClient
from client.input_formats import MessageToHub
from utils.aiotools import run_in_sync
from utils.crypting import get_rsa_keys
from utils.device_id import get_device_id_from_key_pub

BASE_DIR = os.environ['BASE_DIR']
RSA_KEYS_PATH = os.environ.get('RSA_KEYS_PATH', 'keys')

HUB_HOST = os.environ.get('HUB_HOST', '37.157.193.224')
HUB_PORT = os.environ.get('HUB_PORT', '80')
HUB_WS_URL = 'ws://{}:{}/ws'.format(HUB_HOST, HUB_PORT)

APP_HOST = os.environ.get('APP_HOST', '127.0.0.1')
APP_PORT = os.environ.get('APP_PORT', '8000')


env = Environment(loader=PackageLoader(__name__))
app = Sanic(__name__)


HUB_CLIENT = None


def _get_client_id():
    if HUB_CLIENT and HUB_CLIENT.websocket:
        return get_device_id_from_key_pub(HUB_CLIENT.key_pub)

    return '-'


def _render_to_string(template_name, params):
    template = env.get_template(template_name)
    return html(template.render(**params))


def get_list_of_payloads():
    list_of_payloads = {}

    for message_type in os.listdir(os.path.join(BASE_DIR, 'payloads')):
        list_of_payloads[message_type] = set()
        for payload_name in os.listdir(os.path.join(BASE_DIR, 'payloads', message_type)):
            if payload_name[-5:] == '.json':
                list_of_payloads[message_type].add(payload_name)
            else:
                list_of_payloads[f"{message_type}/{payload_name}"] = set()
                for _name in os.listdir(os.path.join(BASE_DIR, 'payloads', message_type, payload_name)):
                    list_of_payloads[f"{message_type}/{payload_name}"].add(_name)

    return list_of_payloads


def get_payload(message_type, payload_name):
    with open(os.path.join(BASE_DIR, 'payloads', message_type, *payload_name.split('/')), 'r') as f:
        return f.read()


@app.route('/', methods=['GET', 'POST'])
async def index_reponse(request):
    # redirects to first valid payload schema
    if request.method == 'GET':
        for message_type, payloads in get_list_of_payloads().items():
            for payload_name in payloads:
                return redirect(f"/payload/{ message_type }/{ payload_name }")

    # process data and send them
    debug_mode = bool(request.form.get('debug_mode', False))
    device_id = request.form['device_id'][0]
    message_type = request.form['message_type'][0]
    payload = request.form['payload'][0]

    outbound_message = MessageToHub(message_type=debug_mode and f"debug-{message_type}" or message_type,
                                    data=json.loads(payload),
                                    receiver_id=device_id)

    if HUB_CLIENT:
        await HUB_CLIENT.send(outbound_message.message_type,
                              outbound_message.data.plain,
                              outbound_message.location,
                              outbound_message.receiver_id,
                              _wait_for_confirmation=True)

    return _render_to_string('index.html', {
        'local_client_id': _get_client_id(),
        'list_of_payloads': get_list_of_payloads(),
        'debug_mode': debug_mode,
        'device_id': device_id,
        'message_type': message_type,
        'payload': payload,
        'outbound_message': outbound_message.plain,
    })


# noinspection PyUnresolvedReferences
@app.route('/payload/<message_type:[a-z/]+>/<payload_name:[A-z0-9-_]+.json>')
async def payload_response(request, message_type, payload_name):
    device_id = request.raw_args.get('device_id', '')
    debug_mode = bool(request.raw_args.get('debug_mode', False))

    return _render_to_string('index.html', {
        'local_client_id': _get_client_id(),
        'list_of_payloads': get_list_of_payloads(),
        'debug_mode': debug_mode,
        'device_id': device_id,
        'message_type': message_type,
        'payload': get_payload(message_type, payload_name),
    })


async def _runserver():
    await app.create_server(host=APP_HOST, port=APP_PORT, debug=True)


async def _connect_client():
    global HUB_CLIENT

    print('Getting local client\'s RSA keys...')
    local_public_key, local_private_key = get_rsa_keys(pair_name='local_client')
    with open(os.path.join(BASE_DIR, RSA_KEYS_PATH, 'hub_rsa.pub'), 'rb') as f:
        hub_public_key = PublicKey.load_pkcs1(f.read(), format='PEM')

    print(f'Starting drone with id "{ get_device_id_from_key_pub(local_public_key) }"')
    HUB_CLIENT = HubClient(HUB_WS_URL, local_public_key, local_private_key, hub_public_key,
                           auto_reconnect=True)
    await HUB_CLIENT.connect()


async def _run():
    await asyncio.gather(_connect_client(),
                         _runserver())


if __name__ == '__main__':
    run_in_sync(_run)()
